//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IGS_ChatR
{
    using System;
    using System.Collections.Generic;
    
    public partial class Signal_Messages
    {
        public int ID { get; set; }
        public string Message { get; set; }
        public int UserID { get; set; }
        public System.DateTime Date { get; set; }
        public int RoomID { get; set; }
    
        public virtual Signal_Users Signal_Users { get; set; }
        public virtual Signal_Room Signal_Room { get; set; }
    }
}
