﻿function showLoading() {
    var Panel = $('#loadingPanel');
    Panel.removeClass("hidden");
}

function hideLoading() {
    var Panel = $('#loadingPanel');
    Panel.addClass("hidden");
}