﻿using IGS_ChatR.Classes.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGS_ChatR.Handlers.Rooms
{
    /// <summary>
    /// Summary description for GetRoomUser
    /// </summary>
    public class GetRoomUser : IHttpHandler
    {
        public class UserModel
        {
            public int ID { get; set; }
            public string Username { get; set; }
            public bool Accepted { get; set; }
        }

        public void ProcessRequest(HttpContext context)
        {
            var RoomID = Convert.ToInt32(context.Request.Params["RoomID"]);
            var en = new SignalREntities();
            var RoomUsers = en.Signal_RoomUserInvite.Where(x => x.RoomID == RoomID).Select(x => x).ToList();
            var AcceptedIDs = RoomUsers.Where(x=>x.Accepted == true).Select(x => x.UserID).ToList();
            var PendingIDs = RoomUsers.Where(x => x.Accepted == false).Select(x => x.UserID).ToList();
            var AcceptedUsers = en.Signal_Users.Where(x => AcceptedIDs.Select(y => y).Contains(x.ID)).ToList();
            var PendingUsers = en.Signal_Users.Where(x => PendingIDs.Select(y => y).Contains(x.ID)).ToList();
            List<UserModel> uml = new List<UserModel>();

            foreach(var au in AcceptedUsers)
            {
                var um = new UserModel();
                um.ID = au.ID;
                um.Username = au.Username;
                um.Accepted = true;

                uml.Add(um);
            }

            foreach(var pu in PendingUsers)
            {
                var um = new UserModel();
                um.ID = pu.ID;
                um.Username = pu.Username;
                um.Accepted = false;

                uml.Add(um);
            }


            context.Response.Write(GenericHelper.JsonMe(uml));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}