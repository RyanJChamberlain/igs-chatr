﻿using IGS_ChatR.Classes.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGS_ChatR.Handlers.Rooms
{
    /// <summary>
    /// Summary description for GetRoomHistory
    /// </summary>
    public class GetRoomHistory : IHttpHandler
    {
        public class Message
        {
            public int ID { get; set; }
            public string Text { get; set; }
            public string User { get; set; }
            public string Date { get; set; }
        }
        public void ProcessRequest(HttpContext context)
        {
            var RoomID = Convert.ToInt32(context.Request.Params["RoomID"]);
            var en = new SignalREntities();
            var Messages = en.Signal_Messages.Where(x => x.RoomID == RoomID).Select(x => x).ToList();
            List<Message> ms = new List<Message>();
            var RoomUsers = en.Signal_RoomUserInvite.Where(x => x.RoomID == RoomID && x.Accepted == true).Select(x => x.UserID).ToList();
            
            var Test = en.Signal_Users.Where(x => RoomUsers.Select(y=>y).Contains(x.ID)).ToList();
            foreach (var mess in Messages)
            {
                var m = new Message();
                m.ID = mess.ID;
                m.Text = mess.Message;
                m.Date = mess.Date.ToString();
                m.User = Test.Where(x => x.ID == mess.UserID).Select(x => x.Username).FirstOrDefault();
                ms.Add(m);
            }
            context.Response.Write(GenericHelper.JsonMe(ms));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}