﻿using IGS_ChatR.Classes.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGS_ChatR.Handlers.Rooms
{
    /// <summary>
    /// Summary description for SendMessage
    /// </summary>
    public class SendMessage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var UserID = Convert.ToInt32(context.Request.Params["UserID"]);
            var RoomID = Convert.ToInt32(context.Request.Params["RoomID"]);
            var Message = context.Request.Params["Message"];
            var en = new SignalREntities();
            var NewMessage = new Signal_Messages();

            NewMessage.Message = Message;
            NewMessage.RoomID = RoomID;
            NewMessage.UserID = UserID;
            NewMessage.Date = DateTime.Now;

            en.Signal_Messages.Add(NewMessage);
            en.SaveChanges();

            var Username = en.Signal_Users.Where(x => x.ID == UserID).Select(x => x.Username).FirstOrDefault();
            context.Response.Write(GenericHelper.JsonMe(Username));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}