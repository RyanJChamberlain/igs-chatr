﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IGS_ChatR.Classes.Helpers;

namespace IGS_ChatR.Handlers
{
    /// <summary>
    /// Summary description for GetMessages
    /// </summary>
    public class GetMessages : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var RoomID = Convert.ToInt32(context.Request.Params["RoomID"]);
            var en = new SignalREntities();
            var Messages = en.Signal_Messages.Where(x => x.RoomID == RoomID).Select(x => x).ToList();
            context.Response.Write(GenericHelper.JsonMe(Messages));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}