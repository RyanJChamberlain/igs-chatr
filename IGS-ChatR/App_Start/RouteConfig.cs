using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;

namespace IGS_ChatR
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Permanent;
            routes.EnableFriendlyUrls(settings);

            routes.MapPageRoute("", "Login", "~/Pages/default.aspx");

            //Admin Pages
            routes.MapPageRoute("", "Admin/Dashboard", "~/Pages/Admin/Dashboard.aspx");

            //User Pages
            routes.MapPageRoute("", "Chat/Room/{ID}", "~/Pages/Chat/Room.aspx");
        }
    }
}
