﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="IGS_ChatR.Pages._default" %>

<%--<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="col-md-12 LoginBox">
            <div class="row InputLayout">
                <div class="col-md-3"></div>
                <div class="col-md-6" style="text-align: center;">
                    <span class="redSubTitle"><b>Login</b></span>
                </div>
            </div>
            <div class="row InputLayout">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <asp:TextBox runat="server" ID="txtUsername" CssClass="form-control" PlaceHolder="Username" Width="100%"></asp:TextBox>
                </div>
            </div>
            <div class="row InputLayout">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" CssClass="form-control" PlaceHolder="Password" Width="100%"></asp:TextBox>
                </div>
            </div>
            <div class="row InputLayout">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <asp:Button runat="server" OnClick="btnLogin_Click" ID="btnLogin" CssClass="btn btn-success" Text="Login" Width="100%"></asp:Button>
                </div>
            </div>
            <div class="row InputLayout">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button type="button" id="btnRegister" data-target="#LoginModal" data-toggle="modal" class="btn btn-primary" style="width: 100%">New? Register Here!</button>
                </div>
            </div>
        </div>
    </div>

    <div id="LoginModal" class="modal fade modalPos" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-md-12" style="text-align: center;">
                    <span class="redSubTitle">Register!</span>
                </div>
            </div>
            <div class="modal-body">
                <div class="row InputLayout">
                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="txtRegisterEmail" CssClass="form-control" Width="100%" PlaceHolder="Email"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row InputLayout">
                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="txtRegisterUsername" CssClass="form-control" Width="100%" PlaceHolder="Username"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row InputLayout">
                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="txtRegisterPassword" TextMode="Password" CssClass="form-control" Width="100%" PlaceHolder="Password"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row InputLayout">
                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="txtRegisterForename" CssClass="form-control" Width="100%" PlaceHolder="Forename"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row InputLayout">
                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <asp:TextBox runat="server" ID="txtRegisterSurname" CssClass="form-control" Width="100%" PlaceHolder="Surname"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <asp:Button runat="server" ID="btnSubmitRegister" OnClick="btnSubmitRegister_Click" CssClass="btn btn-success" Width="100%" Text="Register!" />
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-danger" style="width: 100%" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JsScript" runat="server">
    <script>
        function AlreadyTaken() {
            swal(
                 'Sorry!',
                 'That Username is already taken!',
                 'error'
                     )
        }

        function showLoad() {
            showLoading();
        }
    </script>
</asp:Content>
