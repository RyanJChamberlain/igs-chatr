﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IGS_ChatR.Classes.Security;
using System.Data.Entity.Validation;
using IGS_ChatR.Classes.Helpers;

namespace IGS_ChatR.Pages
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmitRegister_Click(object sender, EventArgs e)
        {
            var en = new SignalREntities();
            var Username = txtRegisterUsername.Text;
            var Password = txtRegisterPassword.Text;
            var Email = txtRegisterEmail.Text;
            var Forename = txtRegisterForename.Text;
            var Surname = txtRegisterSurname.Text;
            var Hashes = Encryption.SHA256Hash(Password);

            var AvailableCheck = en.Signal_Users.Where(x => x.Username == Username).FirstOrDefault();

            if (AvailableCheck == null)
            {
                var NewUser = new Signal_Users();
                NewUser.Created = DateTime.Now;
                NewUser.Email = Email;
                NewUser.Username = Username;
                NewUser.Password = Hashes;
                NewUser.Forename = Forename;
                NewUser.Surname = Surname;

                en.Signal_Users.Add(NewUser);
                en.SaveChanges();
            }
            else
            {

            }

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            var en = new SignalREntities();
            var EnteredUsername = txtUsername.Text;
            var Password = txtPassword.Text;
            var Hashes = Encryption.SHA256Hash(Password);

            var User = en.Signal_Users.Where(x => x.Username == EnteredUsername && x.Password == Hashes).Select(x => x).FirstOrDefault();
            if(User != null)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Key", "showLoad();", true);
                CookieHelper.Set("ID", User.ID.ToString(), 0, 0, 1);

                if(User.Forename == null)
                {
                    CookieHelper.Set("NewUser", "true", 0, 0, 1);
                }

                var UserGroups = en.Signal_GroupUsers.Where(x => x.UserID == User.ID).Select(x => x).ToList();
                var AdminCheck = UserGroups.Where(x => x.GroupID == 1).Select(x => x).FirstOrDefault();
                if(AdminCheck != null)
                {
                    Response.Redirect("Admin/Dashboard");
                }
            }
        }
    }
}