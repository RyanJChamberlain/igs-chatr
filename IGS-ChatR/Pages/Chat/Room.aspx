﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="Room.aspx.cs" Inherits="IGS_ChatR.Pages.Chat.Room" %>


<%@ Register Src="~/Controls/UserPanel.ascx" TagPrefix="uc1" TagName="UserPanel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <uc1:UserPanel runat="server" ID="UserPanel" />

    <div class="row">
        <div class="col-md-9">
            <div class="col-md-12 RoomPanel">
                <div class="conversation" style="overflow-y: scroll;">
                    <ul id="messages">
                    </ul>
                </div>
            </div>
            <div class="col-md-12 ChatBar">
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="txtMessageWindow" ClientIDMode="Static" Width="100%" Height="120px" CssClass="form-control noResize" TextMode="MultiLine"></asp:TextBox>
                </div>
                <div class="col-md-2">
                    <button type="button" id="btnSendMessage" onclick="SendMessage(); return false" value="Send" style="width: 100%; height: 120px;" class="btn btn-success">Send!</button>
                </div>
            </div>
        </div>
        <div class="col-md-3 RoomSideBar">
            <div class="UserList" style="overflow-y:scroll;">
                <ul id="Users" style="list-style-type: none;">

                </ul>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JsScript" runat="server">
    <script>
        $(document).ready(function () {            
            var RoomID = getValueAtIndex(5);
            console.log(RoomID);
            $.ajax({
                url: '/Handlers/Rooms/GetRoomHistory.ashx',
                data: {
                    "RoomID": RoomID
                },
                success: function (data) {
                    var output = JSON.parse(data);
                    console.log(output);
                    $.each(output, function (index, o) {                       
                        var encodedName = $('<div />').text(o.User).html();
                        var encodedMsg = $('<div />').text(o.Text).html();
                        $('#messages').append('<li><strong>' + encodedName
                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');                        
                    })
                    $.ajax({
                        url: '/Handlers/Rooms/GetRoomUser.ashx',
                        data: {
                            "RoomID": RoomID
                        },
                        success: function (data) {                            
                            var output = JSON.parse(data);
                            $.each(output, function (index, o) {
                                var i = "";
                                console.log(o.Accepted);
                                if (o.Accepted == true) {
                                    i += '<li><i class="fa fa-check" aria-hidden="true"></i>';
                                }
                                else {
                                    i += '<li><i class="fa fa-times" aria-hidden="true"></i>';
                                }
                                var encodedName = $('<div />').text(o.Username).html();
                                i += '<strong>' + encodedName + '</strong></li>'
                                $('#Users').append(i);
                            })
                        }
                    })
                    hideLoading();
                }
            })

           
        })

        function getValueAtIndex(index) {
            var str = window.location.href;
            return str.split("/")[index];
        }

        function SendMessage() {

        }

        $(function () {
            var UserID = 1;
            var RoomID = getValueAtIndex(5);

            // Declare a proxy to reference the hub. 
            var chat = $.connection.chatHub;
            // Create a function that the hub can call to broadcast messages.
            chat.client.broadcastMessage = function (name, message) {
                // Html encode display name and message. 
                var encodedName = $('<div />').text(name).html();
                var encodedMsg = $('<div />').text(message).html();
                // Add the message to the page. 
                $('#messages').append('<li><strong>' + encodedName
                    + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
            };
            // Get the user name and store it to prepend to messages.
            // Set initial focus to message input box.  
            $('#txtMessageWindow').focus();
            // Start the connection.
            $.connection.hub.start().done(function () {
                $('#btnSendMessage').click(function () {

                    var Message = $('#txtMessageWindow').val();
                    var Username = "";
                    $.ajax({
                        url: '/Handlers/Rooms/SendMessage.ashx',
                        data: {
                            "UserID": UserID,
                            "RoomID": RoomID,
                            "Message": Message
                        },
                        success: function (data) {
                            var o = JSON.parse(data);
                            Username = o;

                            // Call the Send method on the hub. 
                            chat.server.send(Username, $('#txtMessageWindow').val());
                            // Clear text box and reset focus for next comment. 
                            $('#txtMessageWindow').val('').focus();
                        }
                    })

                   
                });
            });
        })


    </script>
</asp:Content>
