﻿using IGS_ChatR.Classes.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IGS_ChatR.Pages.Admin
{
    public partial class Dashboard : System.Web.UI.Page
    {
        private int _UserID
        {
            get { return ViewState["UserID"] != null ? (int)ViewState["UserID"] : 0; }
            set { ViewState["UserID"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UserHelper.LoggedIn();
                _UserID = Convert.ToInt32(CookieHelper.Get("ID"));
                PopRooms();
                
            }
        }

        

        protected void PopRooms()
        {
            var en = new SignalREntities();
            var YourRooms = en.Signal_RoomUserInvite.Where(x => x.ID == _UserID && x.Accepted == true).Select(x => x.Signal_Room).ToList();
            List<RoomModel> Model = new List<RoomModel>();
            foreach(var y in YourRooms)
            {
                var Users = en.Signal_RoomUserInvite.Where(x => x.Accepted == true).Select(x => x.ID).ToList();
                var Total = Users.Count();
                var m = new RoomModel();
                m.ID = y.ID;
                m.Name = y.Name;
                m.People = (Total > 0) ? Total : 0;

                Model.Add(m);
            }

            rptRooms.DataSource = Model;
            rptRooms.DataBind();
           
        }

        public class RoomModel
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public int People { get; set; }
        }

        protected void btnMakeNewRoom_Click(object sender, EventArgs e)
        {
            var en = new SignalREntities();
            var NewRoom = txtNewRoomName.Text;

            var Check = en.Signal_Room.Where(x => x.Name == NewRoom).Select(x => x).FirstOrDefault();

            if(Check == null)
            {
                var Room = new Signal_Room();
                var Member = new Signal_RoomUserInvite();
                Room.Name = NewRoom;
                Room.Created = DateTime.Now;
                Room.AdminID = _UserID;

                Member.Date = DateTime.Now;
                Member.Accepted = true;
                Member.UserID = _UserID;

                en.Signal_RoomUserInvite.Add(Member);
                en.Signal_Room.Add(Room);
                en.SaveChanges();
                PopRooms();
            }
        }
    }
}