﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Master.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="IGS_ChatR.Pages.Admin.Dashboard" %>

<%@ Register Src="~/Controls/UserPanel.ascx" TagPrefix="uc1" TagName="UserPanel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="update1" runat="server">
        <ContentTemplate>  
            <uc1:UserPanel runat="server" ID="UserPanel" />

                <div class="col-md-6 DashBoardBoxMargin" style="margin-top: 15px;">
                    <div class="DashBoardBodyTall">
                        <div class="row">
                            <div class="col-md-12 redSubTitle">
                                <label>Your Rooms</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="min-height:180px;">
                                <asp:Repeater runat="server" ID="rptRooms" >
                                    <HeaderTemplate>
                                        <div class="row">
                                            <div class="col-md-4" style="text-align: center;">
                                                <label>Name</label>
                                            </div>
                                            <div class="col-md-4" style="text-align: center;">
                                                <label>No. People</label>
                                            </div>
                                            <div class="col-md-4" style="text-align: center;">
                                                <label></label>
                                            </div>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-md-4" style="text-align: center;">
                                                <%# Eval("Name").ToString() %>
                                            </div>
                                            <div class="col-md-4" style="text-align: center;">
                                                <%# Eval("People").ToString() %>
                                            </div>
                                            <div class="col-md-4" style="padding-right: 20px;">
                                                <asp:Button runat="server" ID="btnChat" ClientIDMode="Static" OnClientClick="goToRoom(this); return false" data-roomid='<%# Eval("ID") %>' Text="Chat!" CssClass="btn btn-success" Width="100%" />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 40px;">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-8">
                                <button type="button" id="btnCreateNewRoom" data-target="#makeRoomModal" data-toggle="modal" style="width: 100%" class="btn btn-success">Create New Room!</button>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 DashBoardBoxMargin" style="margin-top: 15px;">
                    <div class="DashBoardBodyTall">
                         <div class="row">
                            <div class="col-md-12 redSubTitle">
                                <label>Private Messages</label>
                            </div>
                        </div>
                    </div>
                </div>
                        
            <div id="makeRoomModal" class="modal fade modalPos">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="col-md-12 redSubTitle">
                            <label>Create a Room!</label>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row InputLayout">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-10" style="text-align: center;">
                                <label style="text-align: center;">We just need a name for your new room and you can start inviting others to join you!</label>
                            </div>
                        </div>
                        <div class="row InputLayout">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="txtNewRoomName" CssClass="form-control" PlaceHolder="New Room Name!" Width="100%"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row InputLayout">
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <asp:Button runat="server" OnClick="btnMakeNewRoom_Click" ID="btnMakeNewRoom" CssClass="btn btn-success" Text="Make Room!" Width="100%" />
                            </div>
                            <div class="col-md-5">
                                <asp:Button runat="server" ID="btnCancelNewRoom" data-dismiss="modal" CssClass="btn btn-danger" Text="Cancel" Width="100%" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
            <asp:Button runat="server" ClientIDMode="Static" ID="btnPopMoreInfo" CssClass="hidden" data-taget="#moreModal" data-toggle="modal" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JsScript" runat="server">
    <script>
        function goToRoom(obj) {
            showLoading();
            var obj = $(obj);
            var RoomID = obj.data("roomid");
            window.location = ("/Chat/Room/" + RoomID);
        }

        function blink() {
            $('#notifications').delay(300).fadeTo(100, 0.5).delay(300).fadeTo(100, 1, blink);
        }

        $(document).ready(function () {
            blink();
        });
    </script>
</asp:Content>
