﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IGS_ChatR.Startup))]
namespace IGS_ChatR
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}