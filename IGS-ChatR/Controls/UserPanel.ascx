﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserPanel.ascx.cs" Inherits="IGS_ChatR.Controls.UserPanel" %>
<style>
    .btnNoStyle {
        background-color: transparent;
        border: none;
        height: 20px;
        width: 20px;
        outline: none;
    }
</style>

<div class="DashPanel" style="margin-top: 50px; margin-bottom: 15px;">
    <div class="col-md-8">
        <div class="col-md-1" style="text-align: right;">
            <i class="fa fa-thumbs-o-up fa-2x" aria-hidden="true"></i>
        </div>
        <div class="col-md-11">
            <asp:Label runat="server" ID="lblWelcomeMessage" CssClass="redSubTitle"></asp:Label>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-6"></div>
        <div class="col-md-2">
            <i id="notifications" class="fa fa-exclamation-circle fa-2x" aria-hidden="true"></i>
        </div>
        <div class="col-md-2">
            <button runat="server" type="button" id="btnDetails" class="btnNoStyle" data-target="#UserDetails" data-toggle="modal"><i class="fa fa-cogs fa-2x" aria-hidden="true"></i></button>
        </div>
        <div class="col-md-2">
            <button runat="server" type="button" id="btnLogout" class="btnNoStyle" onserverclick="Logout"><i class="fa fa-times fa-2x" aria-hidden="true"></i></button>
        </div>
    </div>
</div>

<div id="UserDetails" class="modal fade modalPos">
    <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                </div>
            </div>
        </div>
        <div class="modal-footer">
        </div>
    </div>
</div>


<script>
   
</script>
