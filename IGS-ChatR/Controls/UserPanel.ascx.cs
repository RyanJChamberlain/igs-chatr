﻿using IGS_ChatR.Classes.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IGS_ChatR.Controls
{
    public partial class UserPanel : System.Web.UI.UserControl
    {
        private int _UserID
        {
            get { return ViewState["UserID"] != null ? (int)ViewState["UserID"] : 0; }
            set { ViewState["UserID"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if(CookieHelper.Get("ID") != null)
                {
                    _UserID = Convert.ToInt32(CookieHelper.Get("ID"));
                    var User = UserHelper.GetUserByID(_UserID);
                    lblWelcomeMessage.Text = "Hey, " + User.Forename + " " + User.Surname + "! How things?";
                }
               
            }
        }

        protected void Logout(object sender, EventArgs e)
        {
            CookieHelper.Delete("ID");
            Response.Redirect("~/Login");
        }
    }
}