﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace IGS_ChatR.Classes.Helpers
{
    public class UserHelper
    {
        public static Signal_Users GetUserByID(int ID)
        {
            var en = new SignalREntities();
            var User = en.Signal_Users.Where(x => x.ID == ID).Select(x => x).FirstOrDefault();
            return User;
        }

        public static void LoggedIn()
        {
            if(CookieHelper.Get("ID") == "")
            {                
                HttpContext.Current.Response.Redirect("~/Login");
            }
            else
            {

            }
        }
            
    }
}