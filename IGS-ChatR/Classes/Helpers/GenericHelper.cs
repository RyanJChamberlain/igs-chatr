﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGS_ChatR.Classes.Helpers
{
    public class GenericHelper
    {
        public static string JsonMe(object obj)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer2 = new System.Web.Script.Serialization.JavaScriptSerializer();
            string str = serializer2.Serialize(obj);
            return str;
        }

    }
}