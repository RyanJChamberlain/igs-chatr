﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IGS_ChatR.Classes.Helpers
{
    public static class CookieHelper
    {
        private static HttpContext _context
        {
            get { return HttpContext.Current; }
        }

        public static void Set(string key, string value, int minutes = 0, int days = 0, int hours = 0)
        {
            var c = new HttpCookie(key)
            {
                Value = value,
                HttpOnly = true
            };

            if (minutes != 0)
            {
                c.Expires = DateTime.Now.AddMinutes(minutes);
            }

            if (days != 0)
            {
                c.Expires = DateTime.Now.AddDays(days);
            }

            if (hours != 0)
            {
                c.Expires = DateTime.Now.AddHours(hours);
            }

            _context.Response.Cookies.Add(c);
        }

        public static string Get(string key)
        {
            var value = string.Empty;

            var c = _context.Request.Cookies[key];
            return c != null
                ? _context.Server.HtmlEncode(c.Value).Trim()
                : value;
        }

        public static bool Exists(string key)
        {
            return _context.Request.Cookies[key] != null;
        }

        public static void Delete(string key)
        {
            if (Exists(key))
            {
                var c = new HttpCookie(key) { Expires = DateTime.Now.AddDays(-10), Value = null };
                _context.Response.Cookies.Add(c);
            }
        }

        public static void DeleteAll()
        {
            for (var i = 0; i <= _context.Request.Cookies.Count - 1; i++)
            {
                if (_context.Request.Cookies[i] != null)
                    Delete(_context.Request.Cookies[i].Name);
            }
        }
    }
}