﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace IGS_ChatR.Classes.Security
{
    /// <summary>
    ///     Summary description for Encryption.
    /// </summary>
    public class Encryption
    {
        public static string SHA256Hash(string PlainText)
        {
            var encoder = new UnicodeEncoding();
            var sha256 = new SHA256Managed();

            var MessageBytes = encoder.GetBytes(PlainText);
            var HashData = sha256.ComputeHash(MessageBytes);

            var HashString = "";

            foreach (var b in HashData)
            {
                HashString += string.Format("{0:x2}", b);
            }

            return HashString;
        }


        public static string SHA512Hash(string text)
        {
            var hash = "";
            var alg = SHA512.Create();
            var result = alg.ComputeHash(Encoding.UTF8.GetBytes(text));
            hash = Encoding.UTF8.GetString(result);
            return hash;
        }
    }
}